'''
This module simply defines the weather class, and a broadcast function, to keep the mainfile clean
'''

import sys
from paho.mqtt import publish


class Weather:
	'''
	Weather class, encapsulates weather data at a given instance into one object.
	'''
	time = 0
	summary = ""
	temperature = 0
	realfeel = 0
	humidity = 0

	def __init__(self, time, summary, temperature, realfeel, humidity):
		self.time = time
		self.summary = summary
		self.temperature = int(temperature)
		self.realfeel = int(realfeel)
		self.humidity = humidity * 100

	def broadcast(self, host):
		'''
		Broadcasts self members to MQTT host.
		'''
		try:
			time_message = {'topic': 'weather/time', 'payload': self.time, 'qos': 0, 'retain': True}
			summary_message = {'topic': 'weather/summary', 'payload': self.summary, 'qos': 0, 'retain': True}
			temperature_message = {'topic': 'weather/temperature', 'payload': self.temperature, 'qos': 0, 'retain': True}
			realfeel_message = {'topic': 'weather/realfeel', 'payload': self.realfeel, 'qos': 0, 'retain': True}
			humidity_message = {'topic': 'weather/humidity', 'payload': self.humidity, 'qos': 0, 'retain': True}

			messages = [time_message, summary_message, temperature_message, realfeel_message, humidity_message]
			publish.multiple(messages, hostname=host[0], port=host[1])
		except:
			print("Failure to broadcast! Bailing!")
			sys.exit()

'''
Tests module for pirate-weather-mqtt
'''


import unittest

import json
import yaml
from paho.mqtt import subscribe

import libwet


class TestLibwet(unittest.TestCase):
	'''
	Class test, mandated by the unittest module.
	'''

	def test_class(self):
		'''
		Simple test which checks whether the init function performs well.
		'''
		test_weather = libwet.Weather(1673192855, "Sunny", 10.5, 10.4, 0.85)
		self.assertEqual(test_weather.time, 1673192855, "Time should be returned as is")
		self.assertEqual(test_weather.summary, "Sunny", "Summary should be returned as is")
		self.assertEqual(test_weather.temperature, 10, "Temperature must be rounded down")
		self.assertEqual(test_weather.realfeel, 10, "Realfeel must be rounded down")
		self.assertEqual(test_weather.humidity, 85, "Humidity is expressed in percentage")

	def test_broadcast(self):
		'''
		Simple test which tests the broadcast function.
		Sends to the MQTT broker, gets an echo, and compares the two.
		'''

		with open("config.yaml", "r", encoding='UTF-8') as yamlfile:
			conf = yaml.safe_load(yamlfile)
		host = [conf['broker'], conf['port']]

		# "fake" weather report
		request_file = open("test.json", "r", encoding="UTF-8")
		report = json.loads(request_file.readline())
		request_file.close()
		current = report['currently']
		test_weather = libwet.Weather(current['time'], current['summary'], current['temperature'], current['apparentTemperature'], current['humidity'])
		test_weather.broadcast(host)
		mlist = subscribe.simple(['weather/time', 'weather/summary', 'weather/temperature', 'weather/realfeel', 'weather/humidity'], keepalive=2, msg_count=5, retained=True, hostname=host[0], port=host[1])
		self.assertEqual(mlist[0].payload.decode('UTF-8'), str(test_weather.time))
		self.assertEqual(mlist[1].payload.decode('UTF-8'), str(test_weather.summary))
		self.assertEqual(mlist[2].payload.decode('UTF-8'), str(test_weather.temperature))
		self.assertEqual(mlist[3].payload.decode('UTF-8'), str(test_weather.realfeel))
		self.assertEqual(mlist[4].payload.decode('UTF-8'), str(test_weather.humidity))


unittest.main()

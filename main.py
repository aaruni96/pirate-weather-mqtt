'''
Main program for pirate-weather-mqtt.
'''


import sys
import json
import yaml
import requests
import libwet

# settings for pirate weather api
with open("config.yaml", "r", encoding="UTF-8") as yamlfile:
	conf = yaml.safe_load(yamlfile)
apiKey = conf['apiKey']
location = conf['location']
url = f"https://dev.pirateweather.net/forecast/{apiKey}/{location}?units=si"

# settings for MQTT broker
host = [conf['broker'], conf['port']]

try:
	r = requests.get(url, timeout=5)
except:
	print("Web request failed, bailing!")
	sys.exit()

report = json.loads(r.text)
current = report['currently']

nowWeather = libwet.Weather(
	current['time'], current['summary'],
	current['temperature'],
	current['apparentTemperature'],
	current['humidity']
)
nowWeather.broadcast(host)

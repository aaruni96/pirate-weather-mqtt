# pirate-weather-mqtt

Uses the [pirate weather API](https://pirateweather.net/) to publish weather data over MQTT.

## Requirements

Depends on the following to run :
 - python 3
 - python-json
 - python-yaml
 - python-requests
 - python-paho

## Usage

Rename `config.yaml.sample` to `config.yaml` and adapt it to your usage. Then simply run in a cronjob to publish retained weather data to your broker. Clients can then query the MQTT broker for weather info and not swamp pirate weather with requests.

## ToDo

Right now, we only parse the information I need for my conky setup. But one could parse the minutely / hourly / daily data as well.
 - [ ] minutely
 - [ ] hourly
 - [ ] daily
 - [ ] weather alerts